<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity]
/**
 * @ORM\Table(name="category",options="auto_increment": 100)
 * @ORM\Entity()
 * @ORM\MappedSuperclass()
 *
 **/
class Category
{
    #[ORM\Id]
    #[ORM\CustomIdGenerator]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\CustomIdGenerator
     * @ORM\Column(type="integer")
     */
    private $id = 1;


    /**
     * @ORM\Column(type="string")
     */
    #[ORM\Column]
    private $name;

    /**
     * @var \DateTime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;



    public function __construct()
    {
        $this->created_at = new \DateTime();
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;
        return $this;
    }

    public function toArray() {
        return get_object_vars($this);
    }


}
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;

abstract class AbstractApiController extends AbstractController
{
    protected function buildForm(string $type , $data=null , array $options=[]):FormInterface
    {

        return $this->container->get('form.factory')->createNamed('',$type,$data,$options);
    }

    protected function response($data,$status=\Symfony\Component\HttpFoundation\Response::HTTP_OK){

        return $this->json($data,$status);

    }


}
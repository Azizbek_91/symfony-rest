<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\Type\CategoryType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class CategoryController extends AbstractApiController
{
    #[Route(path: 'api/category/create',name: 'createCategory',methods: ['POST'])]
    public function createAction(UserInterface $user,ManagerRegistry $doctrine,Request $request){

        if(!$user->isAdmin())
            $this->response('Peremision denied',Response::HTTP_FORBIDDEN);


        $form = $this->buildForm(CategoryType::class);

        $form->handleRequest($request);

        if(!$form->isSubmitted() or !$form->isValid())
            $this->response($form,Response::HTTP_BAD_REQUEST);

        $product = $form->getData();

        $doctrine->getManager()->persist($product);

        $doctrine->getManager()->flush();
        $data['success']= 'Category saved';

        return $this->response($data);
    }
    #[Route(path: '/api/get-all-category',name: 'getCategoryAll',methods: ['GET'])]
    public function getAllProductAction(Request $request ,ManagerRegistry $managerRegistry){

        $arr = [];
        $res = $managerRegistry->getRepository(Category::class)->findAll();

        foreach ($res as $item)
            $arr[] = $item->toArray();

        return $this->response($arr);
    }

    #[Route(path: '/api/get-category',name: 'getCategory',methods: ['GET'])]
    public function getProductAction(Request $request ,ManagerRegistry $managerRegistry){

        $res = $managerRegistry->getRepository(Category::class)
            ->find($request->get('id'));

        return $this->response($res->toArray());
    }

    #[Route(path: '/api/remove-category',name: 'rmCategory',methods: ['DELETE'])]
    public function removeCategoryAction(UserInterface $user,Request $request ,ManagerRegistry $managerRegistry){

        if(!$user->isAdmin())
            $this->response('Peremision denied',Response::HTTP_FORBIDDEN);

        $entityManager=$managerRegistry->getManager();
        $res = $managerRegistry->getRepository(Category::class)
            ->find($request->get('id'));

        $entityManager->remove($res);
        $entityManager->flush();
        return $this->response('Category removed');
    }

    #[Route(path: '/api/update-category',name: 'update_category',methods: ['PUT'])]
    public function updateAction(UserInterface $user, Request $request, ManagerRegistry $managerRegistry){

        if(!$user->isAdmin())
            $this->response('Peremision denied',401);

        $category = $managerRegistry
            ->getRepository(CategoryType::class)
            ->find($request->get('id'));

        if (!$category)
            $this->response('Category not found',Response::HTTP_NOT_MODIFIED);

        $data = json_decode($request->getContent(), true);

        $form = $this->createForm('\App\Form\Type\CategoryType', $category);

        $form->submit($data);

        $em = $managerRegistry->getManager();

        $em->persist($category);

        $em->flush();

        return $this->response("Category updated");
    }


}
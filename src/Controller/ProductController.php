<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\Type\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route as Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

class ProductController extends AbstractApiController
{
    public function indexAction(ManagerRegistry $doctorine): Response{

        $products = $doctorine->getRepository(Product::class)->findAll();

        return $this->json($products);
    }

    #[Route(path: "/api/product/create",name: "create",methods:["POST"])]
    public function createProduct(UserInterface $user ,ManagerRegistry $doctrine , Request $request):Response {

        if(!$user->isAdmin())
            $this->response('Peremision denied',Response::HTTP_FORBIDDEN);


        $form = $this->buildForm(ProductType::class);

        $form->handleRequest($request);


        if(!$form->isSubmitted() || !$form->isValid())
            $this->response($form->getErrors(),Response::HTTP_BAD_REQUEST);


        $file = $request->files->get('image');
        $uploads_dir = $this->getParameter('uploads_directory');
        $filename = md5(uniqid()) . '.' .$file->guessExtension();

        $file->move(
            $uploads_dir,
            $filename
        );

        $form->getData()->setImage($filename);

        $product = $form->getData();

        $doctrine->getManager()->persist($product);
        $doctrine->getManager()->flush();

        $data['success']= 'Product saved';

        return $this->response($data);
    }

    #[Route(path: '/api/get-all-product',name: 'getProductAll',methods: ['GET'])]
    public function getAllProductAction(Request $request ,ManagerRegistry $managerRegistry){

        $arr = [];
        $res = $managerRegistry->getRepository(Product::class)->findAll();

        foreach ($res as $item)
            $arr[] = $item->toArray();

        return $this->response($arr);
    }

    #[Route(path: '/api/get-product',name: 'getProduct',methods: ['GET'])]
    public function getProductAction(Request $request ,ManagerRegistry $managerRegistry){

        $res = $managerRegistry->getRepository(Product::class)
            ->find($request->get('id'));

        return $this->response($res->toArray());
    }

    #[Route(path: '/api/remove-product',name: 'rmProduct',methods: ['DELETE'])]
    public function removeProductAction(UserInterface $user, Request $request ,ManagerRegistry $managerRegistry){

        if(!$user->isAdmin())
            $this->response('Peremision denied',Response::HTTP_FORBIDDEN);

        $entityManager=$managerRegistry->getManager();
        $res = $managerRegistry->getRepository(Product::class)
            ->find($request->get('id'));

        $entityManager->remove($res);
        $entityManager->flush();
        return $this->response('Product removed');
    }

    #[Route(path: '/api/update-product',name: 'update_product',methods: ['PUT'])]
    public function updateAction(UserInterface $user, Request $request, ManagerRegistry $managerRegistry){

        if(!$user->isAdmin())
            $this->response('Peremision denied',401);

        $product = $managerRegistry
            ->getRepository(ProductType::class)
            ->find($request->get('id'));

        if (!$product)
            $this->response('Category not found',Response::HTTP_NOT_MODIFIED);

        $data = json_decode($request->getContent(), true);

        $form = $this->createForm('\App\Form\Type\CategoryType', $product);

        $form->submit($data);

        $em = $managerRegistry->getManager();

        $em->persist($product);

        $em->flush();

        return $this->response("Product updated");
    }

}